import { app, setupApp } from './app';
import { stan } from './stan';
import { setupDB } from './db';

const { PORT } = process.env;

stan.on('connect', async () => {
  const context = {
    db: await setupDB(),
    broker: stan,
  };

  setupApp(context);

  app.listen(PORT, () => console.log(`server running on port ${PORT}`));
});
