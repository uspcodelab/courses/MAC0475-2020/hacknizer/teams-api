import express from 'express';
import { v4 as uuid } from 'uuid';

export async function findPersonByID(db, res, id) {
  const person = await db.collection('available_member').findOne({ id });
  console.log(person);
  if (!person) {
    res.status(400).json({ error: `person ${id} does not exist` });
  }
  return person;
}

export function setupPersonRouter(db, broker) {
  const personRouter = express.Router();

  // list all teamless persons
  personRouter.get('/', async (req, res) => {
    const person = await db.collection('available_member').find({}).toArray();
    res.json({ person });
  });

  return personRouter;
}
