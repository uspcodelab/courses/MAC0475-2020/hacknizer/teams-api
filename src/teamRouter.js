import express from 'express';
import { v4 as uuid } from 'uuid';
import { findPersonByID } from './personRouter';

export function setupTeamRouter(db, broker) {
  const teamRouter = express.Router();

  const findTeam = async (req, res, next) => {
    const { uuid } = req.params;
    const team = await db.collection('team').findOne({ uuid });

    if (!team) {
      res.status(400).json({ error: `team ${uuid} does not exist` });
    } else {
      req.body.team = team;
      next();
    }
  };

  const findMember = async (req, res, next) => {
    const { team } = req.body;
    const { id } = req.params;
    const member = team.members.find(member => member.id == id);
    
    if (!member) {
      res.status(400).json({ error: `member ${id} does not exist` });
    } else {
      req.body.member = member;
      next();
    }
  };

  const findPerson = async (req, res, next) => {
    const { id } = req.body;
    const person = await db.collection('available_member').findOne({ id });

    if (!person) {
      res.status(400).json({ error: `person ${id} is not available` });
    } else {
      req.body.person = person;
      next();
    }
  };

  teamRouter.post('/', (req, res) => {
    const team = {
      name: req.body.name,
      hackathon: req.body.hackathon,
      uuid: uuid(),
      members: [],
      project: {}
    };

    broker.publish('team', JSON.stringify(team));

    res.json({ team });
  });

  teamRouter.get('/', async (req, res) => {
    const team = await db.collection('team').find({}).toArray();
    res.json({ team });
  });

  teamRouter.get('/:uuid', findTeam, async (req, res) => {
    const { team } = req.body;
    res.json({ team });
  });

  // deletes a team
  teamRouter.delete('/:uuid', findTeam, async (req, res) => {
    const { team } = req.body;
    broker.publish('team_deleted', JSON.stringify(team));
    res.json({ team });
  });

  // add person to team
  teamRouter.post('/:uuid/member', findTeam, findPerson, async (req, res) => {
    const { team, person } = req.body;
    team.members.push(person);
    broker.publish('member_added', JSON.stringify(person));
    broker.publish('team', JSON.stringify(team));
    res.json({ team });
  });

  // remove person from team
  teamRouter.delete('/:uuid/member/:id', findTeam, findMember, async (req, res) => {
    const { team, member } = req.body;
    team.members.splice(team.members.indexOf(member), 1);
    broker.publish('team', JSON.stringify(team));
    broker.publish('person', JSON.stringify(member));
    res.json({ team });
  });

  teamRouter.post('/:uuid/project', findTeam, async (req, res) => {
    const { team, name, proposal, repositoryURL } = req.body;
    //TODO verificar se a pessoa existe
    team.project = { name, id: uuid(), proposal, repositoryURL};
    broker.publish('team', JSON.stringify(team));
    res.json({ team });
  });

  return teamRouter;
}