import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import {setupTeamRouter} from './teamRouter';
import { setupPersonRouter } from './personRouter';

export const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.disable('x-powered-by');

export async function setupApp({ db, broker }) {
  app.use('/team', setupTeamRouter(db, broker));
  app.use('/person', setupPersonRouter(db, broker));
  
}

