# Intro ao Express

Para execução em conjunto desse projeto com o sink, clone os dois na mesma pasta e utilize o docker-compose-combined.yaml. 
Para isso, utilize os comandos:

```bash
git clone git@gitlab.com:uspcodelab/courses/MAC0475-2020/hacknizer/teams-api.git
git clone git@gitlab.com:uspcodelab/courses/MAC0475-2020/hacknizer/teams-sink.git
ln -s teams-api/docker-compose-combined.yaml ./docker-compose.yaml
cd ..
docker-compose up
```

OBS: sempre rode os comandos do docker-compose nessa mesma pasta, sem entrar em nenhum dos repositórios!

----


```bash
# constrói a imagem
docker-compose build

# roda em modo 'development'
docker-compose up
```

Para rodar os testes

```
docker-compose run server yarn test [--watchAll]
```

> `--watchAll` observa seu código por alterações e, quando elas acontecerem, roda a bateria de testes novamente
